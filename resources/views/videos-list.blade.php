<!doctype html>

<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
        integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous">
    </script>
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="/">Home </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/videos-list">Videos List </a>
                </li>
        </div>
    </nav>

    <div class="container mt-5">
        <div class="row">
            <div class="col-12 mb-3">
                <h4>
                    Converted Videos &nbsp; <small>Sorted By Latest First.</small>
                </h4>
            </div>
            <br>
            <div class="table-responsive">
                <table class="table table-bordered table-striped">
                    <tr>
                        <td>
                            #
                        </td>
                        <td>
                            Video URL
                        </td>
                    </tr>
                    @if($data->isNotEmpty())
                    @foreach ($data as $k => $video)
                        <tr>
                            <td>
                                {{ $k + 1 }}
                            </td>
                            <td>
                                <a href="/play-video/{{ $video->id }}" target="_blank" rel="noopener noreferrer"
                                    class="btn bt-sm btn-success">
                                    Click to Play
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    @endif
                </table>

            </div>
        </div>
    </div>
</body>

</html>
