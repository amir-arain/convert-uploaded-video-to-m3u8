<?php
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::redirect('/', '/home', 301);
Route::get('/home', function () {
    return view('home');
});
Route::get('/videos-list', function () {
    $data = \DB::table('videos')->latest()->get();
    return view('videos-list')->with('data', $data);
});
Route::get('/play-video/{id}', function ($id) {
    $data = \DB::table('videos')->where('id', $id)->first();
    return view('play-videos')->with('data', $data);
});

Route::post('/upload-video', function () {
    $request = \Request();
    if (!$request->hasFile('file')) {
        return redirect()->back()->with([
            'error' => 'Please upload a video.',
        ]);
    }
    try {
        $uploadedFile = $request->file('file');
        $filename = time() . '_' . time() . '.' . $uploadedFile->getClientOriginalExtension();
        $id = \DB::table('videos')->insertGetId([
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        \DB::table('videos')->where('id', $id)->update([
            'original_video' => url('/files/original_video/'.$id.'/'.$filename),
        ]);
        $uploadedFile->move(public_path('/files/original_video/'.$id), $filename);
        $config = [
            'ffmpeg.binaries' => env('FFMPEG_BINARIES'),
            'ffprobe.binaries' => env('FFPROBE_BINARIES'),
            'timeout' => 3600,
            'ffmpeg.threads' => 12,
        ];

        $ffmpeg = Streaming\FFMpeg::create($config, null);
        $video = $ffmpeg->open(public_path('/files/original_video/'.$id.'/'.$filename));


        //A path you want to save a random key to your local machine
        $save_to = public_path('/files/'.$id.'/key');

        //An URL (or a path) to access the key on your website
        $url = url('/files/'.$id.'/key');

        $filename2 = time() . '_' . time();

        $video->hls()
            ->encryption($save_to, $url)
            ->x264()
            ->autoGenerateRepresentations([720])
            ->save(public_path('/files/converted_video/'.$id.'/'.$filename2.'.m3u8'));
        \DB::table('videos')->where('id', $id)->update([
            'converted_video' => url('/files/converted_video/'.$id.'/'.$filename2.'.m3u8'),
            'updated_at' => now(),
        ]);
        return redirect()->back()->with([
            'success' => 'Video uploaded and converted successfully.',
        ]);
    } catch (\Exception $exception) {
        return redirect()->back()->with([
            'error' => 'Some error occured. Please try again and also check the configuration is correct.',
        ]);
    }
});
