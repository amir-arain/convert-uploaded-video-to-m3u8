Steps to setup application

1. PHP 7.3 or greate needed to run project

2. composer install (in project root)

3. create a db in phpmyadmin and set name in .env on project root

4. copy ffmpeg folder in root of C drive 

5. set path in system environment variable to work ffmpeg correctly.


example to set path:

C:\ffmpeg\bin\ffmpeg.exe
C:\ffmpeg\bin\ffprobe.exe


6. sign out and sign in again for the system to fetch environment variable changes

Not: step 3, 4 and 5 are must for windows

(if not windows, download ffmpeg binaries according to your OS and set them accordingly also set path in .env last two variables, if you don't download and set them accordingly system will not work properly)


7. run php artisan migrate in terminal in project root

8. run php artisan serve and use the system on http://localhost:8000 (you can also create a Vhost if you know how to create it.)




